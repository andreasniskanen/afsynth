#!/usr/bin/env python3

## Copyright (c) <2019> <Andreas Niskanen, University of Helsinki>
## 
## 
## 
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
## 
## 
## 
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
## 
## 
## 
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
## THE SOFTWARE.

from pysat.examples.rc2 import RC2
from pysat.formula import WCNF
from pysat.solvers import Glucose3

import os
import sys
import itertools

class AF():

    def __init__(self, args, atts):
        self.args = list(range(len(args)))
        self.int_to_arg = args
        self.arg_to_int = { self.int_to_arg[arg] : arg for arg in self.args }
        self.atts = [(self.arg_to_int[s], self.arg_to_int[t]) for s,t in atts]
        self.var_counter = itertools.count(1)
        self.attackers = { a : [] for a in self.args }
        for a,b in self.atts:
            self.attackers[b].append(a)
        self.arg_accepted_var = { a : next(self.var_counter) for a in self.args }
        self.arg_rejected_var = { a : next(self.var_counter) for a in self.args }
        self.att_exists = { (a,b) : False for a in self.args for b in self.args }
        for a,b in self.atts:
            self.att_exists[(a,b)] = True

    def print(self, int_to_arg):
        for a in self.args:
            print('arg(' + str(int_to_arg[a]) + ').')
        for a,b in self.atts:
            print('att(' + str(int_to_arg[a]) + ',' + str(int_to_arg[b]) + ').')

def add_arg_rejected_var_clauses(af, solver):
    for a in af.args:
        solver.add_clause([-af.arg_rejected_var[a]] + [af.arg_accepted_var[b] for b in af.attackers[a]])
        for b in af.attackers[a]:
            solver.add_clause([af.arg_rejected_var[a], -af.arg_accepted_var[b]])

def add_cf_clauses(af, solver):
    for a,b in af.atts:
        solver.add_clause([-af.arg_accepted_var[a], -af.arg_accepted_var[b]])

def add_adm_clauses(af, solver):
    add_cf_clauses(af, solver)
    add_arg_rejected_var_clauses(af, solver)
    for a in af.args:
        for b in af.attackers[a]:
            solver.add_clause([-af.arg_accepted_var[a], af.arg_rejected_var[b]])

def add_com_clauses(af, solver):
    add_adm_clauses(af, solver)
    for a in af.args:
        solver.add_clause([af.arg_accepted_var[a]] + [-af.arg_rejected_var[b] for b in af.attackers[a]])

def add_stb_clauses(af, solver):
    add_cf_clauses(af, solver)
    for a in af.args:
        solver.add_clause([af.arg_accepted_var[a]] + [af.arg_accepted_var[b] for b in af.attackers[a]])

class Example():

    def __init__(self, id, args):
        self.id = id
        self.arg_included = { arg: False for arg in args }

    def set_type(self, type):
        self.type = type

    def add_argument(self, arg):
        self.arg_included[arg] = True

    def set_semantics(self, sem):
        if sem not in ['cf', 'adm', 'com', 'stb', 'prf']:
            print('Semantics', sem, 'not supported.')
            sys.exit(1)
        self.sem = sem

    def set_weight(self, weight):
        self.weight = weight

    def print(self, int_to_arg):
        print(('Positive' if self.type == '+' else 'Negative'), 'example', self.id, ':')
        print('Arguments :', [int_to_arg[arg] for arg in self.arg_included if self.arg_included[arg] == True])
        print('Weight    :', self.weight)
        print('Semantics :', self.sem)

class SynthesisInstance():

    def __init__(self, args, examples):
        self.args = list(range(len(args)))
        self.int_to_arg = args
        self.arg_to_int = { self.int_to_arg[arg] : arg for arg in self.args }
        self.examples = examples
        self.id_to_example = { ex.id : ex for ex in examples }
        self.var_counter = itertools.count(1)
        self.att_var = { (a,b) : next(self.var_counter) for a in self.args for b in self.args }
        self.var_to_att = { self.att_var[(a,b)] : (a,b) for a in self.args for b in self.args }
        self.ext_var = { ex.id : next(self.var_counter) for ex in examples }
        self.var_to_ext = { self.ext_var[ex.id] : ex.id for ex in examples }
        self.not_defended_var = { (ex.id, source, target) : next(self.var_counter) for ex in examples if ex.type == '+' and (ex.sem == 'com' or ex.sem == 'prf')
                                                                                   for source in self.args if not ex.arg_included[source] \
                                                                                   for target in self.args }
        self.not_defended_var.update({ (ex.id, source, target) : next(self.var_counter) for ex in examples if ex.type == '-' and (ex.sem == 'adm' or ex.sem == 'prf') \
                                                                                        for source in self.args if ex.arg_included[source] \
                                                                                        for target in self.args if not ex.arg_included[target] })
        self.not_defended_var.update({ (ex.id, source, target) : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'com' \
                                                                                        for source in self.args for target in self.args })
        self.exists_not_defended_var = { (ex.id, arg) : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'com' \
                                                                               for arg in self.args if not ex.arg_included[arg] }
        self.not_attacked_var = { (ex.id, arg) : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'stb'
                                                                        for arg in self.args if not ex.arg_included[arg] }
        self.exists_superset_var = { ex.id : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'prf' }
        self.arg_accepted_var = { (ex.id, arg) : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'prf' \
                                                                        for arg in self.args if not ex.arg_included[arg] }
        self.att_exists_and_source_accepted_var = { (ex.id, source, target) : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'prf' \
                                                                                                     for source in self.args if not ex.arg_included[source] \
                                                                                                     for target in self.args if not ex.arg_included[target] }
        self.att_exists_and_target_accepted_var = { (ex.id, source, target) : next(self.var_counter) for ex in examples if ex.type == '-' and ex.sem == 'prf' \
                                                                                                     for source in self.args if not ex.arg_included[source] \
                                                                                                     for target in self.args if not ex.arg_included[target] }

    def print(self):
        print('Arguments :', self.int_to_arg)
        for ex in self.examples:
            ex.print(self.int_to_arg)

def add_not_defended_var_clauses(instance, ex, solver):
    if ex.type == '+' and (ex.sem == 'com' or ex.sem == 'prf'):
        for a in instance.args:
            if not ex.arg_included[a]:
                for b in instance.args:
                    solver.add_clause([-instance.not_defended_var[(ex.id, a, b)], instance.att_var[(b,a)]])
                    for c in instance.args:
                        if ex.arg_included[c]:
                            solver.add_clause([-instance.not_defended_var[(ex.id, a, b)], -instance.att_var[(c,b)]])
                    solver.add_clause([instance.not_defended_var[(ex.id, a, b)], -instance.att_var[(b,a)]] \
                                      + [instance.att_var[(c,b)] for c in instance.args if ex.arg_included[c]])
    elif ex.type == '-' and (ex.sem == 'adm' or ex.sem == 'prf'):
        for a in instance.args:
            if ex.arg_included[a]:
                for b in instance.args:
                    if not ex.arg_included[b]:
                        solver.add_clause([-instance.not_defended_var[(ex.id, a, b)], instance.att_var[(b,a)]])
                        for c in instance.args:
                            if ex.arg_included[c]:
                                solver.add_clause([-instance.not_defended_var[(ex.id, a, b)], -instance.att_var[(c,b)]])
                        solver.add_clause([instance.not_defended_var[(ex.id, a, b)], -instance.att_var[(b,a)]] \
                                          + [instance.att_var[(c,b)] for c in instance.args if ex.arg_included[c]])
    elif ex.type == '-' and ex.sem == 'com':
        for a in instance.args:
            for b in instance.args:
                solver.add_clause([-instance.not_defended_var[(ex.id, a, b)], instance.att_var[(b,a)]])
                for c in instance.args:
                    if ex.arg_included[c]:
                        solver.add_clause([-instance.not_defended_var[(ex.id, a, b)], -instance.att_var[(c,b)]])
                solver.add_clause([instance.not_defended_var[(ex.id, a, b)], -instance.att_var[(b,a)]] \
                                  + [instance.att_var[(c,b)] for c in instance.args if ex.arg_included[c]])

def add_exists_not_defended_var_clauses(instance, ex, solver):
    if ex.type == '-' and ex.sem == 'com':
        for a in instance.args:
            if not ex.arg_included[a]:
                solver.add_clause([-instance.exists_not_defended_var[(ex.id, a)]] + [instance.not_defended_var[(ex.id, a, b)] for b in instance.args])
                for b in instance.args:
                    solver.add_clause([instance.exists_not_defended_var[(ex.id, a)], -instance.not_defended_var[(ex.id, a, b)]])

def add_not_attacked_var_clauses(instance, ex, solver):
    if ex.type == '-' and ex.sem == 'stb':
        for a in instance.args:
            if not ex.arg_included[a]:
                for b in instance.args:
                    if ex.arg_included[b]:
                        solver.add_clause([-instance.not_attacked_var[(ex.id, a)], -instance.att_var[(b,a)]])
                solver.add_clause([instance.not_attacked_var[(ex.id, a)]] + [instance.att_var[(b,a)] for b in instance.args if ex.arg_included[b]])

def add_att_exists_and_source_accepted_var_clauses(instance, ex, solver):
    if ex.type == '-' and ex.sem == 'prf':
        for a in instance.args:
            if not ex.arg_included[a]:
                for b in instance.args:
                    if not ex.arg_included[b]:
                        solver.add_clause([-instance.att_exists_and_source_accepted_var[(ex.id, a, b)], instance.att_var[(a,b)]])
                        solver.add_clause([-instance.att_exists_and_source_accepted_var[(ex.id, a, b)], instance.arg_accepted_var[(ex.id, a)]])
                        solver.add_clause([instance.att_exists_and_source_accepted_var[(ex.id, a, b)], -instance.att_var[(a,b)], -instance.arg_accepted_var[(ex.id, a)]])

def add_att_exists_and_target_accepted_var_clauses(instance, ex, solver):
    if ex.type == '-' and ex.sem == 'prf':
        for a in instance.args:
            if not ex.arg_included[a]:
                for b in instance.args:
                    if not ex.arg_included[b]:
                        solver.add_clause([-instance.att_exists_and_source_accepted_var[(ex.id, a, b)], instance.att_var[(a,b)]])
                        solver.add_clause([-instance.att_exists_and_source_accepted_var[(ex.id, a, b)], instance.arg_accepted_var[(ex.id, b)]])
                        solver.add_clause([instance.att_exists_and_source_accepted_var[(ex.id, a, b)], -instance.att_var[(a,b)], -instance.arg_accepted_var[(ex.id, b)]])

def add_cf_example_clauses(instance, ex, solver):
    if ex.type == '+':
        for a in instance.args:
            if ex.arg_included[a]:
                for b in instance.args:
                    if ex.arg_included[b]:
                        solver.add_clause([-instance.ext_var[ex.id], -instance.att_var[(a,b)]])
    elif ex.type == '-':
        solver.add_clause([instance.ext_var[ex.id]] + [instance.att_var[(a,b)] for a in instance.args if ex.arg_included[a] \
                                                                               for b in instance.args if ex.arg_included[b]])

def add_adm_example_clauses(instance, ex, solver):
    if ex.type == '+':
        add_cf_example_clauses(instance, ex, solver)
        for a in instance.args:
            if ex.arg_included[a]:
                for b in instance.args:
                    if not ex.arg_included[b]:
                        solver.add_clause([-instance.ext_var[ex.id], -instance.att_var[(b,a)]] \
                                          + [instance.att_var[(c,b)] for c in instance.args if ex.arg_included[c]])
    elif ex.type == '-':
        add_not_defended_var_clauses(instance, ex, solver)
        solver.add_clause([instance.ext_var[ex.id]] + [instance.att_var[(a,b)] for a in instance.args if ex.arg_included[a] \
                                                                               for b in instance.args if ex.arg_included[b]] \
                                                    + [instance.not_defended_var[(ex.id,a,b)] for a in instance.args if ex.arg_included[a] \
                                                                                              for b in instance.args if not ex.arg_included[b]] )

def add_com_example_clauses(instance, ex, solver):
    if ex.type == '+':
        add_adm_example_clauses(instance, ex, solver)
        add_not_defended_var_clauses(instance, ex, solver)
        for a in instance.args:
            if not ex.arg_included[a]:
                solver.add_clause([-instance.ext_var[ex.id]] + [instance.not_defended_var[(ex.id, a, b)] for b in instance.args])
    elif ex.type == '-':
        add_not_defended_var_clauses(instance, ex, solver)
        add_exists_not_defended_var_clauses(instance, ex, solver)
        solver.add_clause([instance.ext_var[ex.id]] + [instance.att_var[(a,b)] for a in instance.args if ex.arg_included[a] \
                                                                               for b in instance.args if ex.arg_included[b]] \
                                                    + [instance.not_defended_var[(ex.id, a, b)] for a in instance.args if ex.arg_included[a] \
                                                                                                for b in instance.args if not ex.arg_included[b]] \
                                                    + [-instance.exists_not_defended_var[(ex.id, a)] for a in instance.args if not ex.arg_included[a]])

def add_stb_example_clauses(instance, ex, solver):
    if ex.type == '+':
        add_cf_example_clauses(instance, ex, solver)
        for a in instance.args:
            if not ex.arg_included[a]:
                solver.add_clause([-instance.ext_var[ex.id]] + [instance.att_var[(b,a)] for b in instance.args if ex.arg_included[b]])
    elif ex.type == '-':
        add_not_attacked_var_clauses(instance, ex, solver)
        solver.add_clause([instance.ext_var[ex.id]] + [instance.att_var[(a,b)] for a in instance.args if ex.arg_included[a] \
                                                                               for b in instance.args if ex.arg_included[b]] \
                                                    + [instance.not_attacked_var[(ex.id, a)] for a in instance.args if not ex.arg_included[a]] )

def add_prf_neg_example_clauses(instance, ex, solver):
    if ex.type == '-':
        for a in instance.args:
            for b in instance.args:
                if not ex.arg_included[a] and not ex.arg_included[b]:
                    solver.add_clause([-instance.exists_superset_var[ex.id], -instance.att_var[(a,b)], -instance.arg_accepted_var[(ex.id, a)], -instance.arg_accepted_var[(ex.id, b)]])
                elif not ex.arg_included[a]:
                    solver.add_clause([-instance.exists_superset_var[ex.id], -instance.att_var[(a,b)], -instance.arg_accepted_var[(ex.id, a)]])
                elif not ex.arg_included[b]:
                    solver.add_clause([-instance.exists_superset_var[ex.id], -instance.att_var[(a,b)], -instance.arg_accepted_var[(ex.id, b)]])
        add_att_exists_and_source_accepted_var_clauses(instance, ex, solver)
        add_att_exists_and_target_accepted_var_clauses(instance, ex, solver)
        for a in instance.args:
            if not ex.arg_included[a]:
                for b in instance.args:
                    if not ex.arg_included[b]:
                        solver.add_clause([-instance.exists_superset_var[ex.id], -instance.att_exists_and_target_accepted_var[(ex.id, b, a)]] \
                                          + [instance.att_exists_and_source_accepted_var[(ex.id, c, b)] for c in instance.args if not ex.arg_included[c]] \
                                          + [instance.att_var[(c,b)] for c in instance.args if ex.arg_included[c]])
            else:
                for b in instance.args:
                    if not ex.arg_included[b]:
                        solver.add_clause([-instance.exists_superset_var[ex.id], -instance.att_var[(b,a)]] \
                                          + [instance.att_exists_and_source_accepted_var[(ex.id, c, b)] for c in instance.args if not ex.arg_included[c]] \
                                          + [instance.att_var[(c,b)] for c in instance.args if ex.arg_included[c]])
        solver.add_clause([-instance.exists_superset_var[ex.id]] + [instance.arg_accepted_var[(ex.id, a)] for a in instance.args if not ex.arg_included[a]])

def add_prf_additional_clauses(instance, solver):
    for ex1 in instance.examples:
        if ex1.type == '+' and ex1.sem == 'prf':
            for ex2 in instance.examples:
                if ex2.id != ex1.id and ex2.type == '+' and ex2.sem == 'prf':
                    is_subset = True
                    for a in instance.args:
                        if ex1.arg_included[a] and not ex2.arg_included[a]:
                            is_subset = False
                            break
                    if is_subset:
                        solver.add_clause([-instance.ext_var[ex1.id], -instance.ext_var[ex2.id]])

def add_refinement_clause(instance, solver, af, example_id, model=None):
    if model == None:
        solver.add_clause([-instance.ext_var[example_id]] \
                          + [(-1 if af.att_exists[(a,b)] else 1)*instance.att_var[(a,b)] \
                             for a in instance.args for b in instance.args])
    else:
        labeling = { a : 0 for a in af.args }
        for a in af.args:
            if model[af.arg_accepted_var[a]-1] > 0:
                labeling[a] = 1
            elif model[af.arg_rejected_var[a]-1] > 0:
                labeling[a] = -1
        clause = [-instance.ext_var[example_id]]
        for a in af.args:
            for b in af.args:
                if af.att_exists[(a,b)]:
                    if labeling[a] == 1 and labeling[b] == -1:
                        clause += [-instance.att_var[(a,b)]]
                else:
                    if (labeling[a] == 1 and labeling[b] == 1) or (labeling[a] == 0 and labeling[b] == 1):
                        clause += [instance.att_var[(a,b)]]
        solver.add_clause(clause)

def synthesize(instance, solver, strong=True):
    cegar = False
    for ex in instance.examples:
        if ex.sem == 'cf':
            add_cf_example_clauses(instance, ex, solver)
        elif ex.sem == 'adm':
            add_adm_example_clauses(instance, ex, solver)
        elif ex.sem == 'com':
            add_com_example_clauses(instance, ex, solver)
        elif ex.sem == 'stb':
            add_stb_example_clauses(instance, ex, solver)
        elif ex.sem == 'prf':
            if ex.type == '+':
                add_com_example_clauses(instance, ex, solver)
                cegar = True
            elif ex.type == '-':
                add_adm_example_clauses(instance, ex, solver)
                add_prf_neg_example_clauses(instance, ex, solver)
        if ex.type == '+':
            solver.add_clause([instance.ext_var[ex.id]], weight=ex.weight)
        elif ex.type == '-':
            if ex.sem != 'prf':
                solver.add_clause([-instance.ext_var[ex.id]], weight=ex.weight)
            else:
                solver.add_clause([-instance.ext_var[ex.id], instance.exists_superset_var[ex.id]], weight=ex.weight)
    if not cegar:
        model = solver.compute()
        print('s OPTIMUM FOUND')
        print('o {0}'.format(solver.cost))
        atts = []
        for i in model:
            if i > 0 and i in instance.var_to_att:
                atts += [instance.var_to_att[i]]
        return AF(instance.args, atts)
    else:
        add_prf_additional_clauses(instance, solver)
        count = 0
        while True:
            count += 1
            model = solver.compute()
            atts = [instance.var_to_att[i] for i in model if i > 0 and i in instance.var_to_att]
            sat_prf_pos_examples = [instance.var_to_ext[i] for i in model if i > 0 and i in instance.var_to_ext \
                                                                          and instance.id_to_example[instance.var_to_ext[i]].sem == 'prf' \
                                                                          and instance.id_to_example[instance.var_to_ext[i]].type == '+']
            af = AF(instance.args, atts)
            ok = True
            for i in sat_prf_pos_examples:
                ex = instance.id_to_example[i]
                sat_solver = Glucose3()
                add_com_clauses(af, sat_solver)
                for a in instance.args:
                    if ex.arg_included[a]:
                        sat_solver.add_clause([af.arg_accepted_var[a]])
                sat_solver.add_clause([af.arg_accepted_var[a] for a in instance.args if not ex.arg_included[a]])
                result = sat_solver.solve()
                if result == True:
                    add_refinement_clause(instance, solver, af, i, sat_solver.get_model() if strong else None)
                    ok = False
                    break
            if ok:
                print('Number of iterations:', count)
                print('s OPTIMUM FOUND')
                print('o {0}'.format(solver.cost))
                return af

def parse_instance(filename):
    lines = open(filename, 'r').read().split('\n')
    int_to_arg = [line.replace('arg(', '').replace(').', '') for line in lines if line.startswith('arg')]
    args = list(range(len(int_to_arg)))
    arg_to_int = { int_to_arg[arg] : arg for arg in args }
    examples = {}
    for line in lines:
        if line.startswith('pos'):
            id, arg = line.replace('pos(', '').replace(').', '').split(',')
            if int(id) not in examples:
                examples[int(id)] = Example(int(id), args)
            examples[int(id)].set_type('+')
            examples[int(id)].add_argument(arg_to_int[arg])
        elif line.startswith('neg'):
            id, arg = line.replace('neg(', '').replace(').', '').split(',')
            if int(id) not in examples:
                examples[int(id)] = Example(int(id), args)
            examples[int(id)].set_type('-')
            examples[int(id)].add_argument(arg_to_int[arg])
        elif line.startswith('sem'):
            id, sem = line.replace('sem(', '').replace(').', '').split(',')
            if int(id) not in examples:
                examples[int(id)] = Example(int(id), args)
            examples[int(id)].set_semantics(sem)
        elif line.startswith('weight'):
            id, weight = line.replace('weight(', '').replace(').', '').split(',')
            if int(id) not in examples:
                examples[int(id)] = Example(int(id), args)
            examples[int(id)].set_weight(int(weight))
    return int_to_arg, list(examples.values())

def print_usage():
    print('Usage:', os.path.basename(sys.argv[0]), 'file')
    print('Arguments:')
    print('    file : Input filename for synthesis instance in apx format.')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)
    filename = sys.argv[1]
    strong = True
    if len(sys.argv) > 2:
        if sys.argv[2] != "strong":
            strong = False
    solver = RC2(WCNF())
    args, examples = parse_instance(filename)
    instance = SynthesisInstance(args, examples)
    #instance.print()
    synthesize(instance, solver, strong).print(instance.int_to_arg)
