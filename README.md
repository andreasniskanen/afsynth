AFSynth : Solver for Argumentation Framework Synthesis
======================================================

Version    : 2020.07.12

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki

This version of AFSynth has been implemented using [PySAT](https://pysathq.github.io/),
in particular it makes use of the [RC2](https://pysathq.github.io/docs/html/api/examples/rc2.html) MaxSAT solver.
Please install PySAT by following [the instructions](https://pysathq.github.io/installation.html) before using this software.

Usage
-----

```
python3 afsynth.py file

Arguments:
  file : Input filename for synthesis instance in apx format.
```

Input format
------------

An AF synthesis instance is represented using an input file containing the following predicates.

```
arg(X).      ... X is an argument
pos(i,X).    ... X is in the positive example i
neg(i,X).    ... X is in the negative example i
sem(i,S).    ... the example i has semantics S={cf|adm|stb|com|prf}
weight(i,n). ... the example i has weight n
```

The following example declares three arguments {a,b,c}, two positive examples {a,c} (conflict-free semantics) and {b,c} (stable semantics), and one negative example {a} (admissible semantics). All examples have unit weights.

```
arg(a).
arg(b).
arg(c).
pos(1,a).
pos(1,c).
weight(1,1).
sem(1,cf).
pos(2,b).
pos(2,c).
weight(2,1).
sem(2,stb).
neg(3,a).
weight(3,1).
sem(3,adm).
```

Contact
-------

Please direct any questions, comments, bug reports etc. directly to [the author](mailto:andreas.niskanen@helsinki.fi).
